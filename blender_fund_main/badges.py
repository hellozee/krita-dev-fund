import datetime
import logging
import typing

from django.conf import settings
import django.contrib.auth
import django.contrib.auth.models
from django.utils import timezone
import requests

from .models import BadgerQueuedCall

RETRY_COUNT = 5
POST_TIMEOUT = 30  # seconds

# Internal error codes < 100 because HTTP status codes start at 100.
ERROR_CODE_ALREADY_QUEUED_CALLS = 1
ERROR_CODE_EXCEPTION = 2

# List of (max age of oldest queued item, delay before flushing again) tuples.
# if age >= tuple[0] → flush after tuple[1]
FLUSH_INTERVALS = [
    (datetime.timedelta(minutes=30), datetime.timedelta(seconds=15)),
    (datetime.timedelta(minutes=2), datetime.timedelta(seconds=10)),
    (datetime.timedelta(seconds=30), datetime.timedelta(seconds=5)),
    (datetime.timedelta(seconds=5), datetime.timedelta(seconds=2)),
    (datetime.timedelta(seconds=0), datetime.timedelta(seconds=1)),
]

log = logging.getLogger(__name__)
User = django.contrib.auth.get_user_model()


def change_badge(user, *, grant: str = '', revoke: str = '',
                 session: typing.Optional[requests.Session] = None):
    """Change badge from 'revoke' to 'grant'.

    Either parameter is optional.
    """
    my_log = log.getChild('change_badge')
    assert isinstance(user, User), f'expecting User, not {user!r}'

    if grant == revoke:
        my_log.debug('Nothing to do, grant=revoke=%r', grant)
        return

    if session is None:
        session = badger_session()

    my_log.info('Changing badges for user %s, grant=%r, revoke=%r', user.email, grant, revoke)

    if revoke:
        call = BadgerQueuedCall(
            action='revoke',
            badge=revoke,
            user=user,
        )
        send(call, session=session)

    if grant:
        call = BadgerQueuedCall(
            action='grant',
            badge=grant,
            user=user,
        )
        send(call, session=session)


def badger_session() -> requests.Session:
    """Creates a Requests session for sending to Blender ID."""
    import requests.adapters

    sess = requests.Session()
    sess.mount('https://', requests.adapters.HTTPAdapter(max_retries=5))
    sess.mount('http://', requests.adapters.HTTPAdapter(max_retries=5))
    return sess


def queue_size() -> int:
    """Returns the number of queued calls to Blender ID."""
    return BadgerQueuedCall.objects.count()


def last_delivery_attempt() -> typing.Optional[datetime.datetime]:
    """Determine latest last_delivery_attempt of any queued item."""
    from django.db.models import Max
    queued = BadgerQueuedCall.objects.all()
    aggr = queued.aggregate(Max('last_delivery_attempt'))

    # This will be None if no delivery attempts have been made.
    return aggr['last_delivery_attempt__max']


def flush_time(*, now: typing.Optional[datetime.datetime] = None) -> \
        typing.Optional[datetime.datetime]:
    """Returns the datetime at which this queue should be flushed.

    :returns: the datetime when should be flushed, or None when the queue is empty.
    """
    if queue_size() == 0:
        return None

    last = last_delivery_attempt()
    if last is None:
        if now is not None:
            return now
        return timezone.now()

    delay = flush_delay()
    if delay is None:
        return None
    return last + delay


def flush_delay(*, now: typing.Optional[datetime.datetime] = None) -> \
        typing.Optional[datetime.timedelta]:
    """Returns the delay after which this queue should be flushed.

    Add this to last_flush_attempt to have an absolute datetime.

    :returns: the timedelta after it should be flushed, or None when the queue is empty.
    """
    if queue_size() == 0:
        return None

    if now is None:
        now = timezone.now()

    queue = BadgerQueuedCall.objects
    oldest = queue.order_by('created').first()
    if oldest is None:
        # Last item got flushed since we checked the queue size.
        return None

    age = now - oldest.created
    for agelimit, delay in FLUSH_INTERVALS:
        if age >= agelimit:
            return delay

    # Fall back to rapid re-flushing.
    return datetime.timedelta(seconds=1)


def send(call: BadgerQueuedCall, *, session: typing.Optional[requests.Session] = None):
    """Sends a message to the Badger API.

    :param call: the Badger API call to perform. Does not have to be persisted
        in the database.
    :param session: the Requests session to use for sending. This allows
        multiple webhooks to the same host to share a TCP/IP connection,
        and allows the caller control over the number of retries.
    """
    my_log = log.getChild('send')

    if session is None:
        session = badger_session()

    def record_error(status_code: int, error_msg: str):
        """Records an error by either creating a new queued call or updating one."""
        call.error_code = status_code
        call.error_msg = error_msg
        call.save()

    queue_size_ = queue_size()
    if queue_size_ > 0 and not call.pk:
        # Refuse sending new items when there are items already queued.
        # Sending out queued items is okay, though.
        my_log.warning('immediately queueing Blender ID Badger call because we already '
                       'have %d items queued.', queue_size_)
        record_error(ERROR_CODE_ALREADY_QUEUED_CALLS,
                     f'queueing because we already have {queue_size_} items queued')
        return

    url = call.url
    auth_headers = {
        'Authorization': 'Bearer ' + settings.BLENDER_ID.get('BADGER_API_SECRET', 'secret-not-set')
    }
    try:
        my_log.debug('sending to %s', url)

        call.last_delivery_attempt = timezone.now()
        resp = session.post(url, headers=auth_headers, timeout=POST_TIMEOUT)
        if resp.status_code >= 400:
            my_log.warning('error calling Blender ID Badger API, HTTP %s, queueing',
                           resp.status_code)
            record_error(resp.status_code, resp.text or '')
            return
    except (IOError, OSError) as ex:
        my_log.warning('error calling Blender ID Badger API, exception %s, queueing', ex)
        record_error(ERROR_CODE_EXCEPTION, str(ex))
        return

    # If we're here, the send was a success.
    if call.pk:
        my_log.info('dequeueing Blender ID Badger call to %s', url)
        call.delete()


def flush():
    """Tries to deliver all queued calls to the Blender ID Badger API."""
    my_log = log.getChild('flush')

    queued_count = queue_size()
    if queued_count == 0:
        my_log.debug('nothing to flush')
        return

    my_log.info('flushing %d queued item(s) to the Blender ID Badger API', queued_count)

    # Order by 'created' to keep items in the correct order. This is important
    # for subscription statuses for example (the store takes away the subscription
    # role when payment is due, and gives it back when paid, and those should be
    # handled in the correct order).
    sess = badger_session()
    for item in BadgerQueuedCall.objects.order_by('created'):
        send(item, session=sess)
