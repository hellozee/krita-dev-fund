from django.urls import path, reverse_lazy
from django.views.generic import RedirectView

from .views import development, errors, settings

urlpatterns = [
    # The settings/ path prefix is shared between this module and looper.urls.
    path('settings/', settings.settings_home, name='settings_home'),
    path('settings/receipts/', settings.settings_receipts, name='settings_receipts'),
    path('settings/receipts/<int:order_id>', settings.ReceiptView.as_view(),
         name='settings_receipt'),
    path('settings/receipts/blender-fund-<int:order_id>.pdf',
         settings.ReceiptPDFView.as_view(),
         name='settings_receipt_pdf'),
    path('settings/membership/', RedirectView.as_view(
        url=reverse_lazy('settings_home'),
        permanent=False)),
    path('settings/membership/<int:membership_id>', settings.MembershipView.as_view(),
         name='settings_membership_edit'),
    path('settings/membership/<int:membership_id>/cancel', settings.CancelMembershipView.as_view(),
         name='settings_membership_cancel'),

    # Flexible payment
    path('settings/membership/<int:membership_id>/extend', settings.ExtendMembershipView.as_view(),
         name='membership_extend'),
    path('settings/membership/<int:membership_id>/extend-done',
         settings.ExtendMembershipDoneView.as_view(),
         name='membership_extend_done'),
    path('settings/membership/<int:membership_id>/extend-not-possible',
         settings.ExtendMembershipNotPossibleView.as_view(),
         name='membership_extend_not_possible'),

    path('my-permissions', development.my_permissions),
    path('test-error/500', errors.test_error_500),
]
