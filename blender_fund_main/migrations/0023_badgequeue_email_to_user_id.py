# Generated by Django 2.1.3 on 2018-12-18 16:45

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('blender_fund_main', '0022_membership_is_managed'),
    ]

    operations = [
        migrations.AddField(
            model_name='badgerqueuedcall',
            name='user',
            field=models.ForeignKey(help_text='The owner of the badge', null=True,
                                    on_delete=django.db.models.deletion.CASCADE,
                                    to=settings.AUTH_USER_MODEL),
        ),
    ]
