# Generated by Django 2.1.2 on 2018-10-12 13:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blender_fund_main', '0016_memlevel_badge_to_image'),
    ]

    operations = [
        migrations.AlterField(
            model_name='membershiplevel',
            name='image',
            field=models.ImageField(blank=True, help_text='Image shown on the front page', null=True, upload_to='membershiplevels/'),
        ),
    ]
