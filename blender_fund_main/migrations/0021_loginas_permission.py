from django.db import migrations, models


def create_loginas_permission(apps, schema_editor):
    ContentType = apps.get_model('contenttypes', 'ContentType')
    Permission = apps.get_model('auth', 'Permission')
    User = apps.get_model('auth', 'User')

    content_type = ContentType.objects.get_for_model(User)
    Permission.objects.create(
        codename='can_loginas',
        name='Can Login As Other User',
        content_type=content_type,
    )


def delete_loginas_permission(apps, schema_editor):
    ContentType = apps.get_model('contenttypes', 'ContentType')
    Permission = apps.get_model('auth', 'Permission')
    User = apps.get_model('auth', 'User')

    content_type = ContentType.objects.get_for_model(User)
    Permission.objects.filter(codename='can_loginas', content_type=content_type).delete()


class Migration(migrations.Migration):
    dependencies = [
        ('blender_fund_main', '0020_wagtail_add_social_media_image'),
    ]

    operations = [
        migrations.RunPython(create_loginas_permission, delete_loginas_permission),
    ]
