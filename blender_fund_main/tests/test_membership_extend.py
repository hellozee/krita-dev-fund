import copy
import datetime
import logging
from pathlib import Path
import tempfile
from urllib.parse import urljoin
from unittest import mock

from dateutil.relativedelta import relativedelta
from django.urls import reverse
from django.test import override_settings
from django.utils.timezone import utc

from looper.gateways import PaymentMethodInfo
from looper.money import Money
from looper.tests import AbstractLooperTestCase
from .. import models

log = logging.getLogger(__name__)


class MembershipExtendTest(AbstractLooperTestCase):
    fixtures = AbstractLooperTestCase.fixtures + ['mock-gateway']

    def setUp(self):
        super().setUp()

        self.subs = self.create_active_subscription()
        self.subs.next_payment = datetime.datetime(2200, 1, 2, 3, 4, 5, tzinfo=utc)
        self.subs.save()

        self.mem = self.subs.membership
        self.url = reverse('membership_extend', kwargs={'membership_id': self.mem.id})
        self.success_url = reverse('membership_extend_done', kwargs={'membership_id': self.mem.id})
        self.client.force_login(self.user)

    def assertIsRedirectToSuccess(self, resp):
        self.assertEqual(302, resp.status_code, resp.content.decode())
        self.assertEqual(self.success_url, resp['Location'])

    def test_extend_happy(self):
        orig_next_payment = copy.copy(self.mem.subscription.next_payment)

        with mock.patch('looper.gateways.MockableGateway.generate_client_token') as mock_gct:
            mock_gct.return_value = 'je-moeder-token'
            resp = self.client.get(self.url)
        self.assertEqual(200, resp.status_code)

        def mock_pmc(mock_self, payment_method_nonce: str, gateway_customer_id: str):
            self.assertEqual(payment_method_nonce, 'fake-payment-nonce')
            self.assertEqual(gateway_customer_id, 'mock-customer-id')
            mock_payment_info = mock.Mock(spec=PaymentMethodInfo)
            mock_payment_info.method_type = PaymentMethodInfo.Type.CREDIT_CARD
            mock_payment_info.token = 'mock-paymeth-token'
            mock_payment_info.recognisable_name.return_value = 'Mocked Credit Card'
            mock_payment_info.type_for_database.return_value = 'cc'
            return mock_payment_info

        def mock_ts(mock_self, paymeth_token: str, amount: Money):
            self.assertEqual(paymeth_token, 'mock-paymeth-token')
            self.assertEqual(amount, Money('EUR', 4700))
            return 'mock-transaction-id'

        with mock.patch('looper.gateways.MockableGateway.customer_create') as mock_cc, \
                mock.patch('looper.gateways.MockableGateway.payment_method_create', new=mock_pmc), \
                mock.patch('looper.gateways.MockableGateway.transact_sale', new=mock_ts):
            mock_cc.side_effect = ['mock-customer-id']
            resp = self.client.post(self.url, {
                'gateway': 'mock',
                'payment_method_nonce': 'fake-payment-nonce',
                'currency': self.subs.currency,
                'price': '47.00',
            })
        self.assertIsRedirectToSuccess(resp)
        self.subs.refresh_from_db()

        # The membership is for €25 per month and paid €47.
        bought = relativedelta(months=1, days=26, hours=18, minutes=2, seconds=52)
        self.assertAlmostEqualDateTime(orig_next_payment + bought, self.subs.next_payment)

    # TODO: test for subscriptionless memberships.
    # TODO: test for next_payment in the past.
