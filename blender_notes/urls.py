from django.urls import path

from . import views

app_name = 'notes'
urlpatterns = [
    path('<app_label>/<model_name>/<int:object_id>',
         views.NotesView.as_view(), name='for-object'),
    path('delete/<app_label>/<model_name>/<int:object_id>/<int:note_id>',
         views.DeleteNoteView.as_view(), name='delete-note'),
]
