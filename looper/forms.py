import logging
import typing

from django import forms
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
import django.contrib.auth.forms as auth_forms

from . import exceptions, models, model_mixins, money, form_fields

log = logging.getLogger(__name__)


class BlenderClassesFormMixin:
    """Adds the CSS classes we need to Django forms."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = f'field-{field_name}'

    def __getitem__(self, field_name: str):
        """Ugly hack to get custom CSS classes in the row container element.

        This will break when Django changes its internal structure.
        """
        from django.forms import BoundField

        # BaseForm._html_output() uses `self[field_name]` to obtain the bound field.
        # To hook into that. we override __getitem__().
        bound_field = super().__getitem__(field_name)  # type: ignore
        if not isinstance(bound_field, BoundField):
            return bound_field

        # `bound_field.css_classes()` is used by BaseForm._html_output() to obtain the
        # CSS classes that are used by the <li> element (when using form.as_ul()).
        # Fortunately it accepts extra CSS classes, but unfortunately we cannot pass
        # anything to the call. Instead, we replace that function with our own, and
        # inject the extra classes there.
        orig_css_classes = bound_field.css_classes

        def css_classes(extra_class: typing.Optional[str] = None) -> str:
            widget_classes: str = bound_field.field.widget.attrs.get('class', '')
            if extra_class:
                widget_classes = f'{widget_classes} {extra_class}'
            return orig_css_classes(widget_classes)

        bound_field.css_classes = css_classes
        return bound_field


class AddressForm(BlenderClassesFormMixin, forms.ModelForm):
    class Meta:
        model = models.Address
        exclude = ['category', 'user']


class BasePaymentForm(forms.Form):
    payment_method_nonce = forms.CharField(initial='set-in-javascript', widget=forms.HiddenInput())
    gateway = form_fields.GatewayChoiceField()


class BaseCheckoutForm(BasePaymentForm):
    # These are used when a payment fails, so that the next attempt to pay can reuse
    # the already-created subscription and order.
    subscription_pk = forms.CharField(widget=forms.HiddenInput(), required=False)
    order_pk = forms.CharField(widget=forms.HiddenInput(), required=False)


class CheckoutForm(BaseCheckoutForm, AddressForm):
    class Meta(AddressForm.Meta):
        pass


class PaymentMethodReplaceForm(BasePaymentForm):
    pass


class ChangePaymentMethodForm(BasePaymentForm):
    next_url_after_done = forms.CharField(widget=forms.HiddenInput())


class CustomerForm(BlenderClassesFormMixin, forms.ModelForm):
    class Meta:
        model = models.Customer
        exclude = ['user', 'vat_number', 'tax_exempt']


class StateMachineMixin(forms.ModelForm):
    """Mix-in for forms that handle looper.model_mixins.StateMachineMixin objects."""

    def clean_status(self):
        assert isinstance(self.instance, model_mixins.StateMachineMixin)

        old_status = self.instance.status
        new_status = self.cleaned_data['status']
        if old_status == new_status:
            # Always accept the current status.
            return new_status

        if not self.instance.may_transition_to(new_status):
            raise ValidationError(f'Status transition {old_status.title()} → '
                                  f'{new_status.title()} is not allowed.')
        return new_status


class PaymentMethodLimitMixin(forms.ModelForm):

    def __init__(self, *args, instance: typing.Optional[models.Subscription] = None, **kwargs) \
            -> None:
        if not instance:
            # When there is no instance, we don't know whose payment methods to list,
            # so it's better to not list them at all.
            invalid_user_id = -1
            queryset = models.PaymentMethod.objects.filter(user_id=invalid_user_id)
        else:
            queryset = instance.user.paymentmethod_set
        self.base_fields['payment_method'].queryset = queryset
        super().__init__(*args, instance=instance, **kwargs)


class SubscriptionAdminForm(StateMachineMixin, PaymentMethodLimitMixin, forms.ModelForm):
    class Meta:
        model = models.Subscription
        exclude = ['id']

    def clean_collection_method(self) -> str:
        payment_method = self.cleaned_data['payment_method']
        collection_method = self.cleaned_data['collection_method']

        if self.instance.collection_method != collection_method and \
                'managed' in {self.instance.collection_method, collection_method}:
            raise ValidationError('Toggling between managed and non-managed is not '
                                  'possible at the moment.')

        if collection_method != 'automatic':
            return collection_method
        if not payment_method:
            raise ValidationError('Automatic payment requires a payment method.')
        return collection_method


class OrderAdminForm(StateMachineMixin, PaymentMethodLimitMixin, forms.ModelForm):
    class Meta:
        model = models.Order
        exclude = ['id']


class AdminTransactionRefundForm(forms.ModelForm):
    class Meta:
        model = models.Order
        exclude = ['id']

    refund_amount = form_fields.MoneyFormField(
        required=False,
        help_text='Amount to refund.')

    log = log.getChild('AdminTransactionRefundForm')

    def clean_refund_amount(self):
        refund_in_cents = self.cleaned_data['refund_amount']
        refund = money.Money(self.instance.currency, refund_in_cents)

        if not refund:
            # No refund is fine.
            return

        refundable = self.instance.refundable
        if refund > refundable:
            raise ValidationError(
                f'Cannot refund more than {refundable.with_currency_symbol()}')
        if refund.cents < 0:
            raise ValidationError(f'Cannot refund negative amounts')
        return refund

    def save(self, commit=True):
        """Save the model and process the refund if successful."""
        instance: models.Transaction = super().save(commit=commit)
        refund: money.Money = self.cleaned_data['refund_amount']
        if not refund:
            self.log.debug('Not processing empty refund for transaction pk=%r', self.instance.pk)
            return instance

        self.log.debug('Processing refund of %s for transaction pk=%r', refund, self.instance.pk)
        try:
            instance.refund(refund)
        except exceptions.GatewayError as ex:
            self.log.exception('Error refunding %s for transaction pk=%r: %s',
                               refund, self.instance.pk, ex)
            raise ValidationError(f'The refund could not be processed: {ex}')

        return instance


class EmailRequiredMixin:
    cleaned_data: dict
    instance: User

    def clean_email(self) -> str:
        if self.cleaned_data['email']:
            return self.cleaned_data['email']

        try:
            if self.instance.customer and self.instance.customer.billing_email:
                # If the user has no email but it does have a billing email,
                # just use that instead.
                return self.instance.customer.billing_email
        except models.Customer.DoesNotExist:
            pass

        from django.core.exceptions import ValidationError
        raise ValidationError('Every user should have an email address')


class AdminUserChangeForm(EmailRequiredMixin, auth_forms.UserChangeForm):
    pass


class AdminUserCreationForm(EmailRequiredMixin, auth_forms.UserCreationForm):
    pass
