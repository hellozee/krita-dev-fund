import logging
import datetime
import typing
from unittest import mock

from django.utils import timezone
from django.test import override_settings
from dateutil.relativedelta import relativedelta

from . import AbstractLooperTestCase

from ..clock import Clock
from .. import models, exceptions, signals, admin_log

# Margin for clock ticks (the tick happens at the given time + this margin).
# Just to stabilise floating point comparisons.
tick_delay = datetime.timedelta(seconds=0.25)

# Retry charging failed orders after this delta.
retry_after = relativedelta(minutes=15)

log = logging.getLogger(__name__)


@override_settings(LOOPER_ORDER_RETRY_AFTER=retry_after)
class AbstractClockTest(AbstractLooperTestCase):
    fixtures = AbstractLooperTestCase.fixtures + ['mock-gateway', 'systemuser']

    # Start time of activated subscriptions.
    start_time = datetime.datetime(2006, 5, 4, 3, 2, 1, tzinfo=timezone.utc)

    def setUp(self):
        super().setUp()

        # The signal itself is passed to the connected function in the 'signal' kwarg,
        # so we can check all signals via one receiver.
        self.signals = mock.Mock()
        signals.automatic_payment_succesful.connect(self.signals)
        signals.automatic_payment_soft_failed.connect(self.signals)
        signals.automatic_payment_failed.connect(self.signals)
        signals.managed_subscription_notification.connect(self.signals)

    def _activate(self, subs: models.Subscription) -> None:
        # This triggers the next_payment to be start_time + 1 month
        with mock.patch('django.utils.timezone.now') as mock_now:
            mock_now.return_value = self.start_time
            subs.status = 'active'
            subs.save()

    def _clock_tick(self, tick_time: datetime.datetime) -> None:
        """Perform the clock tick one hour after the expected next payment time."""
        log.debug('Ticking clock, tick_time=%s', tick_time)
        with mock.patch('django.utils.timezone.now') as mock_now:
            mock_now.return_value = tick_time + tick_delay
            clock = Clock()
            clock.tick()

    def create_active_subscription(self, **extra) -> models.Subscription:
        subs = self.create_subscription(**extra)
        self._activate(subs)
        subs.refresh_from_db()

        if subs.collection_method == 'managed':
            self.assertIsNone(subs.latest_order())
        else:
            order = subs.generate_order()
            self.assertEqual(0, order.collection_attempts)

        return subs


class RenewalTestCase(AbstractClockTest):
    def test_renew_no_subscriptions(self):
        # This shouldn't crash but also don't do anything.
        clock = Clock()
        clock.tick()

        self.signals.assert_not_called()

    def test_renew_manual_subscription(self):
        subs = self.create_active_subscription()
        subs.collection_method = 'manual'
        subs.save()
        last_order_pk = subs.latest_order().pk
        old_next_payment = subs.next_payment

        tick_time = self.start_time + subs.interval
        self._clock_tick(tick_time)

        # The subscription should not be renewed now.
        subs.refresh_from_db()
        self.assertEqual('on-hold', subs.status)
        self.assertEqual(1, subs.intervals_elapsed)
        self.assertAlmostEqualDateTime(old_next_payment, subs.next_payment)

        # Test the order
        new_order = subs.latest_order()
        self.assertEqual('created', new_order.status)
        self.assertNotEqual(last_order_pk, new_order.pk)

        # Test the transaction, it shouldn't exist.
        self.assertIsNone(new_order.latest_transaction())

        self.signals.assert_not_called()

    @mock.patch('looper.gateways.MockableGateway.transact_sale')
    def test_renew_active_subscription(self, mock_transact_sale):
        subs = self.create_active_subscription()

        # Collect some properties before the clock ticks.
        last_order_pk = subs.latest_order().pk
        payment_token = subs.payment_method.token

        mock_transact_sale.side_effect = ['mocked-transaction-id']
        tick_time = self.start_time + subs.interval
        self._clock_tick(tick_time)

        # The subscription should be renewed now.
        subs.refresh_from_db()
        self.assertEqual('active', subs.status)
        self.assertEqual(1, subs.intervals_elapsed)
        self.assertAlmostEqualDateTime(tick_time + relativedelta(months=1), subs.next_payment)

        # Test the order
        new_order = subs.latest_order()
        self.assertEqual('paid', new_order.status)
        self.assertEqual(1, new_order.collection_attempts)
        self.assertIsNone(new_order.retry_after)
        self.assertNotEqual(last_order_pk, new_order.pk)

        # Test the transaction
        new_trans = new_order.latest_transaction()
        self.assertEqual('succeeded', new_trans.status)
        self.assertEqual(subs.price, new_trans.amount)
        entries_q = admin_log.entries_for(new_trans)
        self.assertEqual(1, len(entries_q))
        self.assertIn('success', entries_q.first().change_message)

        # Test the calls on the gateway
        mock_transact_sale.assert_called_with(payment_token, subs.price)

        # Test the sent signals
        self.signals.assert_called_once_with(
            signal=signals.automatic_payment_succesful,
            sender=new_order, transaction=new_trans)

    @mock.patch('looper.gateways.MockableGateway.transact_sale')
    def test_renew_active_subscription_prev_order_exists(self, mock_transact_sale):
        subs = self.create_active_subscription()

        order = subs.latest_order()
        order.status = 'paid'
        order.save()
        order.refresh_from_db()
        self.assertIsNone(order.retry_after)

        # Collect some properties before the clock ticks.
        last_order_pk = order.pk
        payment_token = subs.payment_method.token

        mock_transact_sale.side_effect = ['mocked-transaction-id']
        tick_time = self.start_time + subs.interval
        self._clock_tick(tick_time)

        # The subscription should be renewed now.
        subs.refresh_from_db()
        self.assertEqual('active', subs.status)
        self.assertEqual(1, subs.intervals_elapsed)
        self.assertAlmostEqualDateTime(tick_time + relativedelta(months=1), subs.next_payment)

        # Test the order
        new_order = subs.latest_order()
        self.assertEqual('paid', new_order.status)
        self.assertEqual(1, new_order.collection_attempts)
        self.assertIsNone(new_order.retry_after)
        self.assertNotEqual(last_order_pk, new_order.pk)

        # Test the transaction
        new_trans = new_order.latest_transaction()
        self.assertEqual('succeeded', new_trans.status)
        self.assertEqual(subs.price, new_trans.amount)

        entries_q = admin_log.entries_for(new_trans)
        self.assertEqual(1, len(entries_q))
        self.assertIn('success', entries_q.first().change_message)

        # Test the calls on the gateway
        mock_transact_sale.assert_called_with(payment_token, subs.price)

        # Test the sent signals
        self.signals.assert_called_once_with(
            signal=signals.automatic_payment_succesful,
            sender=new_order, transaction=new_trans)

    @mock.patch('looper.gateways.MockableGateway.transact_sale')
    def test_transact_sale_gatewayerror(self, mock_transact_sale):
        subs = self.create_active_subscription()

        # Collect some properties before the clock ticks.
        last_order_pk = subs.latest_order().pk
        payment_token = subs.payment_method.token
        old_next_payment = subs.next_payment

        mock_transact_sale.side_effect = [exceptions.GatewayError('mock gateway error')]
        tick_time = self.start_time + subs.interval
        with override_settings(LOOPER_CLOCK_MAX_AUTO_ATTEMPTS=1):
            self._clock_tick(tick_time)
        mock_transact_sale.assert_called_with(payment_token, subs.price)

        # The subscription should not have been renewed.
        subs.refresh_from_db()
        self.assertEqual('on-hold', subs.status)
        self.assertEqual(1, subs.intervals_elapsed)
        self.assertAlmostEqualDateTime(old_next_payment, subs.next_payment)

        # Test the order
        new_order = subs.latest_order()
        self.assertEqual('failed', new_order.status)
        self.assertEqual(1, new_order.collection_attempts)
        self.assertNotEqual(last_order_pk, new_order.pk)
        self.assertIsNone(new_order.retry_after, 'Failed orders should not be retried')

        # Test the transaction
        self.assertEqual(1, new_order.transaction_set.count())
        new_trans = new_order.latest_transaction()

        assert new_trans is not None  # make mypy happy
        self.assertEqual('failed', new_trans.status)
        self.assertEqual(subs.price, new_trans.amount)

        entries_q = admin_log.entries_for(new_trans)
        self.assertEqual(1, len(entries_q))
        self.assertIn('failed', entries_q.first().change_message)

        # Test the sent signals
        self.signals.assert_called_once_with(
            signal=signals.automatic_payment_failed,
            sender=new_order, transaction=new_trans)

    @mock.patch('looper.gateways.MockableGateway.transact_sale')
    def test_retry_failed(self, mock_transact_sale):
        subs = self.create_active_subscription()
        pre_renewal_next_payment = subs.next_payment
        pre_renewal_order_pk = subs.latest_order().pk

        # First call should fail, second should succeed.
        mock_transact_sale.side_effect = [
            exceptions.GatewayError('mock gateway error'),
            'mocked-transaction-id',
        ]

        max_collection_attempts = 2
        first_tick_time = self.start_time + subs.interval
        with override_settings(LOOPER_CLOCK_MAX_AUTO_ATTEMPTS=max_collection_attempts):
            self._clock_tick(first_tick_time)

        # The subscription should still be active after one little failure.
        subs.refresh_from_db()
        self.assertEqual('active', subs.status)
        self.assertEqual(1, subs.intervals_elapsed)
        self.assertAlmostEqualDateTime(pre_renewal_next_payment, subs.next_payment)

        # Test the order
        renewal_order = subs.latest_order()
        first_transaction = renewal_order.transaction_set.earliest()
        failed_trans_pk = first_transaction.pk
        self.assertEqual('soft-failed', renewal_order.status)
        self.assertEqual(1, renewal_order.collection_attempts)
        self.assertNotEqual(pre_renewal_order_pk, renewal_order.pk,
                            'A new order should have been created for the renewal')
        self.assertAlmostEqualDateTime(first_tick_time + retry_after, renewal_order.retry_after)

        payment_token = subs.payment_method.token

        self.signals.assert_called_once_with(
            signal=signals.automatic_payment_soft_failed,
            sender=renewal_order, transaction=first_transaction)
        self.signals.reset_mock()

        # Do another clock tick an hour later. This should pick up on the
        # failed attempt and renew the subscription.
        second_tick_time = first_tick_time + relativedelta(hours=1)
        with override_settings(LOOPER_CLOCK_MAX_AUTO_ATTEMPTS=max_collection_attempts):
            self._clock_tick(second_tick_time)

        # The subscription should be renewed now.
        subs.refresh_from_db()
        self.assertEqual('active', subs.status)
        self.assertAlmostEqualDateTime(
            second_tick_time + relativedelta(months=1),
            subs.next_payment)
        self.assertEqual(1, subs.intervals_elapsed)

        # Test the order
        latest_order = subs.latest_order()
        self.assertEqual('paid', latest_order.status)
        self.assertEqual(2, latest_order.collection_attempts)
        self.assertNotEqual(pre_renewal_order_pk, latest_order.pk)
        self.assertEqual(renewal_order.pk, latest_order.pk,
                         'After failing, the renewal order should be reused.')
        self.assertAlmostEqualDateTime(first_tick_time + retry_after, renewal_order.retry_after)

        # Test the transaction
        new_trans = renewal_order.latest_transaction()
        self.assertEqual(2, renewal_order.transaction_set.count())
        self.assertEqual({failed_trans_pk, new_trans.pk},
                         {t.pk for t in renewal_order.transaction_set.all()})
        self.assertEqual('succeeded', new_trans.status)
        self.assertEqual(subs.price, new_trans.amount)

        entries_q = admin_log.entries_for(new_trans)
        self.assertEqual(1, len(entries_q))
        self.assertIn('success', entries_q.first().change_message)

        self.assertGreater(new_trans.pk, failed_trans_pk,
                           'A new transaction should have been made for the retry.')

        # Test the calls on the gateway
        mock_transact_sale.assert_has_calls([
            mock.call(payment_token, subs.price),
            mock.call(payment_token, subs.price),
        ])

        # Test the sent signals
        self.signals.assert_called_once_with(
            signal=signals.automatic_payment_succesful,
            sender=renewal_order, transaction=new_trans)

    @mock.patch('looper.gateways.MockableGateway.transact_sale')
    def test_double_failure(self, mock_transact_sale):
        subs = self.create_active_subscription()
        old_next_payment = subs.next_payment

        # Both calls should fail.
        mock_transact_sale.side_effect = [
            exceptions.GatewayError('mock gateway error'),
            exceptions.GatewayError('mock gateway error'),
        ]

        max_collection_attempts = 2
        first_tick_time = self.start_time + subs.interval
        with override_settings(LOOPER_CLOCK_MAX_AUTO_ATTEMPTS=max_collection_attempts):
            self._clock_tick(first_tick_time)

        self.signals.assert_called_once_with(
            signal=signals.automatic_payment_soft_failed,
            sender=mock.ANY, transaction=mock.ANY)
        self.signals.reset_mock()

        first_failure_order_pk = subs.latest_order().pk
        payment_token = subs.payment_method.token

        # Do another clock tick an hour later. This should pick up on the
        # failed attempt and fail again.
        second_tick_time = first_tick_time + relativedelta(hours=1)
        with override_settings(LOOPER_CLOCK_MAX_AUTO_ATTEMPTS=max_collection_attempts):
            self._clock_tick(second_tick_time)

        # The subscription should not have been renewed.
        subs.refresh_from_db()
        self.assertEqual('on-hold', subs.status)
        self.assertEqual(1, subs.intervals_elapsed)
        self.assertAlmostEqualDateTime(old_next_payment, subs.next_payment)

        # Test the order
        new_order = subs.latest_order()
        self.assertEqual('failed', new_order.status)
        self.assertEqual(2, new_order.collection_attempts)
        self.assertEqual(first_failure_order_pk, new_order.pk)
        self.assertAlmostEqualDateTime(first_tick_time + retry_after, new_order.retry_after)

        # Test the transaction
        self.assertEqual(2, new_order.transaction_set.count())
        new_trans = new_order.latest_transaction()
        self.assertEqual('failed', new_trans.status)
        self.assertEqual(subs.price, new_trans.amount)
        entries_q = admin_log.entries_for(new_trans)
        self.assertEqual(1, len(entries_q))
        self.assertIn('failed', entries_q.first().change_message)

        # Test the calls on the gateway
        mock_transact_sale.assert_has_calls([
            mock.call(payment_token, subs.price),
            mock.call(payment_token, subs.price),
        ])

        # Test the sent signals
        self.signals.assert_called_once_with(
            signal=signals.automatic_payment_failed,
            sender=new_order, transaction=new_trans)

    @mock.patch('looper.gateways.MockableGateway.transact_sale')
    def test_high_frequency_clock(self, mock_transact_sale):
        subs = self.create_active_subscription()
        pre_renewal_next_payment = subs.next_payment

        # Only one call should be made, because the clock is running faster
        # than the LOOPER_ORDER_RETRY_AFTER setting.
        mock_transact_sale.side_effect = [
            exceptions.GatewayError('mock gateway error'),
        ]

        max_collection_attempts = 2
        first_tick_time = self.start_time + subs.interval
        with override_settings(LOOPER_CLOCK_MAX_AUTO_ATTEMPTS=max_collection_attempts):
            self._clock_tick(first_tick_time)

        # The subscription should still be active after one little failure.
        subs.refresh_from_db()
        self.assertEqual('active', subs.status)
        self.assertAlmostEqualDateTime(pre_renewal_next_payment, subs.next_payment)

        # Test the order
        renewal_order = subs.latest_order()
        self.assertAlmostEqualDateTime(first_tick_time + retry_after, renewal_order.retry_after)

        payment_token = subs.payment_method.token

        # Do another clock tick a minute later. This should skip the retry
        # because it's faster than LOOPER_ORDER_RETRY_AFTER.
        second_tick_time = first_tick_time + relativedelta(minutes=1)
        with override_settings(LOOPER_CLOCK_MAX_AUTO_ATTEMPTS=max_collection_attempts):
            self._clock_tick(second_tick_time)

        # The order should not be touched
        renewal_order.refresh_from_db()
        self.assertEqual('soft-failed', renewal_order.status)
        self.assertEqual(1, renewal_order.collection_attempts)
        self.assertAlmostEqualDateTime(first_tick_time + retry_after, renewal_order.retry_after)

        # Test the calls on the gateway
        mock_transact_sale.assert_has_calls([
            mock.call(payment_token, subs.price),
        ])

        # Test the sent signals
        self.signals.assert_called_once_with(
            signal=signals.automatic_payment_soft_failed,
            sender=renewal_order, transaction=renewal_order.latest_transaction())


class PendingCancellationTestCase(AbstractClockTest):
    def test_pending_cancellation(self):
        subs = self.create_active_subscription()
        last_order_pk = subs.latest_order().pk
        next_payment = subs.next_payment

        # Cancel four days into the subscription.
        with mock.patch('django.utils.timezone.now') as mock_now:
            mock_now.return_value = self.start_time + relativedelta(days=4)
            subs.cancel_subscription()

        subs.refresh_from_db()
        self.assertEqual('pending-cancellation', subs.status)

        # Perform the clock tick one hour after the next payment time.
        tick_time = self.start_time + subs.interval
        self._clock_tick(tick_time)

        # The subscription should be cancelled now.
        subs.refresh_from_db()

        # Test the subscription
        self.assertEqual('cancelled', subs.status)
        self.assertAlmostEqualDateTime(next_payment, subs.next_payment)

        # Test the order
        last_order = subs.latest_order()
        self.assertEqual(last_order_pk, last_order.pk, 'Cancelling should not create an order')


class ManagedSubscriptionTest(AbstractClockTest):

    def _test_renew_managed(self, last_notification: typing.Optional[datetime.datetime]):
        subs = self.create_active_subscription(collection_method='managed')
        subs.next_payment = self.start_time
        subs.last_notification = last_notification
        subs.save()

        self.assertIsNone(subs.latest_order())
        old_next_payment = subs.next_payment

        # Tick after the next_payment has passed.
        tick_time = self.start_time + relativedelta(minutes=10)
        self._clock_tick(tick_time)

        # The subscription should still be active and unchanged.
        subs.refresh_from_db()
        self.assertEqual('active', subs.status)
        self.assertAlmostEqualDateTime(old_next_payment, subs.next_payment)
        self.assertAlmostEqualDateTime(tick_time, subs.last_notification)

        # Test the order, shouldn't be created all of a sudden.
        self.assertIsNone(subs.latest_order())

        self.signals.assert_called_once_with(
            signal=signals.managed_subscription_notification,
            sender=subs)

    def test_renew_managed_subscription(self):
        self._test_renew_managed(self.start_time - relativedelta(minutes=1))

    def test_renew_managed_subscription_no_last_notif(self):
        self._test_renew_managed(None)

    def test_renew_managed_subscription_already_notified(self):
        subs = self.create_active_subscription()
        subs.collection_method = 'managed'

        # Next payment has passed, but notification has already happened:
        subs.next_payment = self.start_time
        subs.last_notification = subs.next_payment + relativedelta(seconds=1)
        subs.save()

        last_order_pk = subs.latest_order().pk
        old_next_payment = subs.next_payment
        old_last_notif = subs.last_notification

        # Tick after the next_payment has passed.
        tick_time = self.start_time + relativedelta(minutes=10)
        self._clock_tick(tick_time)

        # The subscription should still be active and unchanged.
        subs.refresh_from_db()
        self.assertEqual('active', subs.status)
        self.assertAlmostEqualDateTime(old_next_payment, subs.next_payment)
        self.assertAlmostEqualDateTime(old_last_notif, subs.last_notification)

        self.assertEqual(last_order_pk, subs.latest_order().pk)

        self.signals.assert_not_called()
