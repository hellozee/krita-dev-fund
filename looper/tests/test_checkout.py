import typing
from unittest import mock

from django.contrib.auth.models import User
from django.dispatch import receiver
from django.urls import reverse, reverse_lazy

from looper.exceptions import GatewayError
from .. import models, signals, gateways
from . import AbstractLooperTestCase


class AbstractCheckoutTestCase(AbstractLooperTestCase):
    fixtures = ['devfund', 'testuser', 'systemuser']
    valid_payload = {
        'gateway': 'braintree',
        'payment_method_nonce': 'fake-valid-nonce',
        'full_name': 'Erik von Namenstein',
        'company': 'Nöming Thingéys',
        'street_address': 'Scotland Pl',
        'locality': 'Amsterdongel',
        'postal_code': '1025 ET',
        'region': 'Worbelmoster',
        'country': 'NL',
    }
    checkout_url = reverse_lazy('looper:checkout',
                                kwargs={'plan_id': 2, 'plan_variation_id': 5})

    def setUp(self):
        super().setUp()
        self.user = User.objects.get(email='harry@blender.org')


class CheckoutTestCase(AbstractCheckoutTestCase):

    def setUp(self):
        super().setUp()

        # Do a GET first to mimick the normal request flow.
        self.client.force_login(self.user)
        r = self.client.get(self.checkout_url)
        self.assertEqual(r.status_code, 200)

    def test_checkout_create_missing_required_fields(self):
        self.client.force_login(self.user)

        payload = self.valid_payload.copy()
        del payload['full_name']
        r = self.client.post(self.checkout_url, data=payload)

        # Django doesn't use a different status code for failed form submissions.
        self.assertEqual(200, r.status_code)
        self.assertNothingHappened()

    def assertNothingHappened(self):
        self.assertEqual(0, models.Subscription.objects.count())
        self.assertEqual(0, models.Order.objects.count())
        self.assertEqual(0, models.Transaction.objects.count())

    def test_checkout_create_invalid_plan(self):
        self.client.force_login(self.user)
        url = reverse_lazy('looper:checkout', kwargs={'plan_id': 200, 'plan_variation_id': 5})
        r = self.client.post(url, data=self.valid_payload)
        self.assertEqual(r.status_code, 404)

    def test_checkout_create_consumed_nonce(self):
        self.client.force_login(self.user)
        payload = {
            **self.valid_payload,
            'payment_method_nonce': 'fake-consumed-nonce',
        }
        r = self.client.post(self.checkout_url, data=payload)
        self.assertEqual(200, r.status_code)
        self.assertNothingHappened()

    def test_checkout_create_valid_subscription(self):
        created_subs: typing.Optional[models.Subscription] = None

        @receiver(signals.subscription_activated)
        def subs_activated(sender: models.Subscription, **kwargs):
            nonlocal created_subs
            created_subs = sender

        self.client.force_login(self.user)
        url = reverse('looper:checkout', kwargs={'plan_id': 2, 'plan_variation_id': 6})
        r = self.client.post(url, data=self.valid_payload)
        self.assertEqual(302, r.status_code, r.content.decode())

        self.assertIsNotNone(created_subs)
        self.assertEqual('active', created_subs.status)

        for key in self.client.session.keys():
            self.assertFalse(key.startswith('PAYMENT_GATEWAY_'),
                             f'Did not expect {key!r} in the session')

        # We should now be able to get the 'done' view for this order.
        order = created_subs.latest_order()
        transaction = order.latest_transaction()
        success_url = reverse('looper:checkout_done',
                              kwargs={'transaction_id': transaction.pk, })
        self.assertEqual(success_url, r['Location'])
        resp = self.client.get(success_url)
        self.assertEqual(200, resp.status_code)

    def test_bank(self):
        sig_receiver = mock.Mock()
        signals.subscription_activated.connect(sig_receiver)
        self.assertEqual(0, models.Subscription.objects.count())

        self.client.force_login(self.user)
        url = reverse('looper:checkout', kwargs={'plan_id': 2, 'plan_variation_id': 6})
        resp = self.client.post(url, data={
            **self.valid_payload,
            'gateway': 'bank',
        })
        self.assertEqual(302, resp.status_code, resp.content.decode())

        sig_receiver.assert_not_called()
        created_subs = models.Subscription.objects.first()
        self.assertIsNotNone(created_subs)
        self.assertEqual('on-hold', created_subs.status)

        order = created_subs.latest_order()
        assert order is not None
        self.assertEqual('created', order.status)
        self.assertIsNone(created_subs.latest_order().latest_transaction())

        for key in self.client.session.keys():
            self.assertFalse(key.startswith('PAYMENT_GATEWAY_'),
                             f'Did not expect {key!r} in the session')

        # We should now be able to get the 'done' view for this order.
        success_url = reverse('looper:transactionless_checkout_done',
                              kwargs={'gateway_name': 'bank', 'pk': order.pk, })
        self.assertEqual(success_url, resp['Location'])
        resp = self.client.get(success_url)
        self.assertEqual(200, resp.status_code)


class MockedCheckoutTest(AbstractCheckoutTestCase):
    fixtures = AbstractCheckoutTestCase.fixtures + ['mock-gateway']

    valid_payload = {
        **AbstractCheckoutTestCase.valid_payload,
        'gateway': 'mock',
    }

    def setUp(self):
        super().setUp()

        # Do a GET first to mimick the normal request flow.
        self.client.force_login(self.user)

        with mock.patch('looper.gateways.MockableGateway.generate_client_token') as mock_gct:
            mock_gct.return_value = 'mock-client-auth-token'
            r = self.client.get(self.checkout_url)
        self.assertEqual(r.status_code, 200)

    def test_processor_declined(self):
        # The rest of the test assumes there aren't any order/subscription/transaction objects yet.
        self.assertEqual(0, models.Subscription.objects.count())
        self.assertEqual(0, models.Order.objects.count())
        self.assertEqual(0, models.Transaction.objects.count())

        with mock.patch('looper.gateways.MockableGateway.customer_create') as mock_cc, \
                mock.patch('looper.gateways.MockableGateway.payment_method_create') as mock_pmc, \
                mock.patch('looper.gateways.MockableGateway.transact_sale') as mock_ts:
            mock_cc.return_value = 'mock-customer-id'

            mock_paymeth = mock.Mock(spec=gateways.PaymentMethodInfo)
            mock_paymeth.token = 'mock-payment-token'
            mock_paymeth.recognisable_name.return_value = 'mock-recognisable-name'
            mock_paymeth.type_for_database.return_value = 'cc'
            mock_pmc.return_value = mock_paymeth

            mock_ts.side_effect = [GatewayError('Processor Declined')]

            r = self.client.post(self.checkout_url, data=self.valid_payload)

        # This should render the same page, with an error message.
        content = r.content.decode()
        self.assertEqual(200, r.status_code, content)
        self.assertIn('Processor Declined', content)

        # The transaction must be created, and thus also its order and subscription.
        self.assertEqual(1, models.Subscription.objects.count())
        self.assertEqual(1, models.Order.objects.count())
        self.assertEqual(1, models.Transaction.objects.count())

        subs_pk = models.Subscription.objects.first().pk
        order_pk = models.Order.objects.first().pk
        self.assertIn(f'<input type="hidden" name="subscription_pk" value="{subs_pk}"', content)
        self.assertIn(f'<input type="hidden" name="order_pk" value="{order_pk}"', content)

        # Paying correctly should not create a new subscription + order, but just pay for
        # the one that was already created.
        with mock.patch('looper.gateways.MockableGateway.payment_method_create') as mock_pmc, \
                mock.patch('looper.gateways.MockableGateway.transact_sale') as mock_ts:
            mock_cc.return_value = 'mock-customer-id'

            mock_paymeth = mock.Mock(spec=gateways.PaymentMethodInfo)
            mock_paymeth.token = 'mock-another-payment-token'
            mock_paymeth.recognisable_name.return_value = 'mock-another-recognisable-name'
            mock_paymeth.type_for_database.return_value = 'pa'
            mock_pmc.return_value = mock_paymeth

            mock_ts.return_value = 'mock-transaction-id'
            r = self.client.post(self.checkout_url, data={
                **self.valid_payload,
                'subscription_pk': subs_pk,
                'order_pk': order_pk,
            })

        content = r.content.decode()
        self.assertEqual(302, r.status_code, content)

        self.assertEqual(1, models.Subscription.objects.count())
        self.assertEqual(1, models.Order.objects.count())
        self.assertEqual(2, models.Transaction.objects.count())


class PayExistingOrder(AbstractCheckoutTestCase):
    def test_pay_for_failed_order(self):
        subs = self.create_on_hold_subscription()
        order = subs.generate_order()

        activated_subs: typing.Optional[models.Subscription] = None

        @receiver(signals.subscription_activated)
        def subs_activated(sender: models.Subscription, **kwargs):
            nonlocal activated_subs
            activated_subs = sender

        self.client.force_login(self.user)
        url = reverse('looper:checkout_existing_order', kwargs={'order_id': order.pk})
        r = self.client.post(url, data=self.valid_payload)
        self.assertEqual(302, r.status_code, r.content.decode())

        self.assertIsNotNone(activated_subs)
        self.assertEqual('active', activated_subs.status)

        for key in self.client.session.keys():
            self.assertFalse(key.startswith('PAYMENT_GATEWAY_'),
                             f'Did not expect {key!r} in the session')


class DefaultPlanVariationTest(AbstractCheckoutTestCase):
    url = reverse_lazy('looper:checkout_new', kwargs={'plan_id': 2})

    def test_euro_ipv6(self):
        from .test_preferred_currency import EURO_IPV6

        self.client.force_login(self.user)
        resp = self.client.get(self.url, REMOTE_ADDR=EURO_IPV6)
        self.assertEqual(302, resp.status_code)

        # The redirect should point to the preferred EUR/monthly variation.
        checkout_url = reverse('looper:checkout', kwargs={'plan_id': 2, 'plan_variation_id': 5})
        self.assertEqual(checkout_url, resp['Location'])

    def test_usa_ipv6_behind_proxy(self):
        from .test_preferred_currency import USA_IPV6

        self.client.force_login(self.user)
        resp = self.client.get(self.url, REMOTE_ADDR='::1', HTTP_X_FORWARDED_FOR=USA_IPV6)
        self.assertEqual(302, resp.status_code)

        # The redirect should point to the preferred USD/monthly variation.
        checkout_url = reverse('looper:checkout', kwargs={'plan_id': 2, 'plan_variation_id': 6})
        self.assertEqual(checkout_url, resp['Location'])
