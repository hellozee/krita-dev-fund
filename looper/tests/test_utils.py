import unittest

from django.http import HttpRequest

from looper import utils

REMOTE_IPV6 = '2001:888:0:9::99'
REMOTE_IPV4 = '194.109.6.66'

LOCAL_PROXY_IPV6 = 'fe80::42:47:327'
LOCAL_PROXY_IPV4 = '10.0.2.22'


class TestGetClientIP(unittest.TestCase):
    @staticmethod
    def req(remote_addr='', forwarded_for='') -> HttpRequest:
        request = HttpRequest()
        if remote_addr:
            request.META['REMOTE_ADDR'] = remote_addr
        if forwarded_for:
            request.META['HTTP_X_FORWARDED_FOR'] = forwarded_for
        return request

    def test_simple(self):
        self.assertEqual('', utils.get_client_ip(self.req()))
        self.assertEqual(REMOTE_IPV6, utils.get_client_ip(self.req(REMOTE_IPV6)))
        self.assertEqual(REMOTE_IPV4, utils.get_client_ip(self.req(REMOTE_IPV4)))

    def test_behind_proxy(self):
        self.assertEqual(REMOTE_IPV6, utils.get_client_ip(self.req(LOCAL_PROXY_IPV4, REMOTE_IPV6)))
        self.assertEqual(REMOTE_IPV4, utils.get_client_ip(self.req(LOCAL_PROXY_IPV6, REMOTE_IPV4)))

    def test_invalid_addr(self):
        self.assertEqual('', utils.get_client_ip(self.req(f'{REMOTE_IPV6},{LOCAL_PROXY_IPV4}')))
        self.assertEqual('', utils.get_client_ip(self.req(f'{REMOTE_IPV4},{LOCAL_PROXY_IPV6}')))
