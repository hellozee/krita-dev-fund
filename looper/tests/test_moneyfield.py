import json

from django.core import serializers

from django.utils.datastructures import MultiValueDict

from .. import models, forms
from ..money import Money, CurrencyMismatch
from . import AbstractLooperTestCase, AbstractBaseTestCase


class MoneyFieldTest(AbstractBaseTestCase):
    fixtures = ['testuser', 'devfund']

    def test_create_instance(self):
        pv = models.PlanVariation(plan_id=1, currency='EUR', price=Money('EUR', 155))
        self.assertEqual(Money('EUR', 155), pv.price)

        s = models.Subscription(user_id=14, plan_id=1, currency='EUR', price=Money('EUR', 1))
        self.assertEqual(Money('EUR', 0), s.tax)

    def test_store_load(self):
        m = models.Subscription(user_id=14, plan_id=1, currency='EUR', price=Money('EUR', 155))
        m.save()

        loaded = models.Subscription.objects.first()
        self.assertEqual(Money('EUR', 155), loaded.price)
        self.assertEqual(Money('EUR', 0), loaded.tax)

    def test_inconsistent_currencies(self):
        with self.assertRaises(CurrencyMismatch):
            models.Subscription(user_id=14,
                                plan_id=1,
                                currency='EUR',
                                price=Money('EUR', 155),
                                tax=Money('USD', 4))

        s = models.Subscription(user_id=14, plan_id=1, currency='EUR')
        with self.assertRaises(CurrencyMismatch):
            s.price = Money('HRK', 1348)

    def test_serialize(self):
        m = models.Subscription(user_id=14, plan_id=1, currency='EUR', price=Money('EUR', 155))
        data = serializers.serialize('json', [m])
        as_json = json.loads(data)[0]

        fields = as_json['fields']
        self.assertIn('currency', fields)
        self.assertEqual('EUR', fields['currency'])
        self.assertIn('price', fields)
        self.assertEqual('EUR\u00A01.55', fields['price'])

        result = list(serializers.deserialize('json', data))
        self.assertEqual(m.currency, result[0].object.currency)
        self.assertEqual(m.price, result[0].object.price)


class FormChangedDataTest(AbstractLooperTestCase):

    def test_changed_data(self):
        order = self.create_subscription().generate_order()
        request_post = MultiValueDict({
            'user': [self.user.id + 1],
            'status': [order.status],
            'name': [order.name],
            'payment_method': [order.payment_method_id],
            'collection_method': [order.collection_method],
            'currency': [order.currency],
            'price': [order.price.decimals_string],
            'tax': [order.tax.decimals_string],
            'tax_type': [''],
            'tax_region': [''],
            'paid_at_0': [''],
            'paid_at_1': [''],
            'retry_after_0': [''],
            'retry_after_1': [''],
            'email': [order.email],
            'billing_address': [order.billing_address],
            'transaction_set-TOTAL_FORMS': ['0'],
            'transaction_set-INITIAL_FORMS': ['0'],
            'transaction_set-MIN_NUM_FORMS': ['0'],
            'transaction_set-MAX_NUM_FORMS': ['0'],
            '_continue': ['Save and continue editing']})

        request_files = MultiValueDict({})
        form = forms.OrderAdminForm(request_post, request_files, instance=order)

        form.full_clean()
        self.assertEqual(['user'], form.changed_data)
