import typing

import django.db.models
import django.conf

# noinspection PyUnresolvedReferences
from django.contrib.admin.models import ADDITION, CHANGE, DELETION


def attach_log_entry(
        instance: django.db.models.Model,
        message: str,
        action_flag: int = CHANGE,
        user_id: typing.Optional[int] = None,
) -> None:
    """Attach a log entry to this model, for in the admin 'history' page.

    :param instance:
    :param message:
    :param action_flag: Either ADDITION, CHANGE, or DELETION, defaults to CHANGE.
    :param user_id: Optionally, the user who performs the logged action.
        Can be None when the action is performed by the system.
    """
    from django.contrib.admin.models import LogEntry
    from django.contrib.contenttypes.models import ContentType

    if user_id is None:
        user_id = django.conf.settings.LOOPER_SYSTEM_USER_ID

    LogEntry.objects.log_action(
        user_id=user_id,
        content_type_id=ContentType.objects.get_for_model(type(instance)).pk,
        object_id=instance.id,
        object_repr=str(instance),
        action_flag=action_flag,
        change_message=message)


def entries_for(instance: django.db.models.Model) -> django.db.models.QuerySet:
    """Build a query for all log entries attached to an instance."""

    from django.contrib.admin.models import LogEntry
    from django.contrib.contenttypes.models import ContentType

    return LogEntry.objects.filter(
        content_type_id=ContentType.objects.get_for_model(type(instance)).pk,
        object_id=instance.id)
