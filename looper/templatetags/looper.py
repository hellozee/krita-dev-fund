from django import template

register = template.Library()


@register.filter
def pricing(pricable) -> str:
    if pricable is None:
        return '-'

    price: str = pricable.price.with_currency_symbol_nonocents()

    if pricable.interval_length == 1:
        return f'{price} / {pricable.interval_unit}'
    return f'{price} / {pricable.interval_length}\u00A0{pricable.interval_unit}s'
