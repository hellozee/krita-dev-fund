def get_client_ip(request):
    """Returns the IP of the request, accounting for the possibility of being
    behind a proxy.
    """
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR', None)
    if x_forwarded_for:
        # X_FORWARDED_FOR returns client1, proxy1, proxy2,...
        return x_forwarded_for.split(', ', 1)[0].strip()

    remote_addr = request.META.get('REMOTE_ADDR', '')
    if not remote_addr:
        return ''

    # REMOTE_ADDR can also be 'ip1,ip2' if people mess around with HTTP
    # headers (we've seen this happen). Don't trust anything in that case.
    if ',' in remote_addr:
        return ''

    return remote_addr


def make_absolute_url(url: str) -> str:
    """Turn a host-absolute URL into a fully absolute URL.

    For simplicity, assumes we're using HTTPS and not running on some URL prefix.
    """
    from django.contrib.sites.models import Site
    from urllib.parse import urljoin

    site: Site = Site.objects.get_current()
    return urljoin(f'https://{site.domain}/', url)
