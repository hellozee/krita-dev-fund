import logging

from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver, Signal

from blender_id_oauth_client import signals as bid_signals

# Sender is the Subscription.
subscription_activated = Signal(providing_args=['old_status'])
subscription_deactivated = Signal(providing_args=['old_status'])
# Sent when next_payment < now and last_notification < next_payment:
managed_subscription_notification = Signal()

# Sender is the Order that's being processed.
automatic_payment_succesful = Signal(providing_args=['transaction'])
automatic_payment_soft_failed = Signal(providing_args=['transaction'])
automatic_payment_failed = Signal(providing_args=['transaction'])

log = logging.getLogger(__name__)


@receiver(bid_signals.user_created)
def set_customer_fullname(sender, instance: User, oauth_info: dict, **kwargs):
    """Create a Customer when a new User is created via OAuth"""
    my_log = log.getChild('create_customer')
    if not instance.customer:
        my_log.info('NOT updating user full_name of user %s, as they have no Customer')
        return

    my_log.info('Updating Customer full_name as result of OAuth login of %s', instance.pk)
    instance.customer.full_name = oauth_info['full_name']
    instance.customer.save()


@receiver(post_save, sender=User)
def create_customer(sender, instance: User, created, **kwargs):
    from looper.models import Customer

    if not created:
        return

    my_log = log.getChild('create_customer')
    try:
        customer = instance.customer
    except Customer.DoesNotExist:
        pass
    else:
        my_log.debug('Newly created User %d already has a Customer %d, not creating new one',
                     instance.pk, customer.pk)
        return

    my_log.info('Creating new Customer due to creation of user %s', instance.pk)
    Customer.objects.create(user=instance, billing_email=instance.email)
