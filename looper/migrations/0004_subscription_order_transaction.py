# Generated by Django 2.1.1 on 2018-09-20 15:42

from django.db import migrations, models
import django.db.models.deletion
import django_countries.fields
import looper.models


class Migration(migrations.Migration):

    dependencies = [
        ('looper', '0003_gateway_name'),
    ]

    operations = [
        migrations.CreateModel(
            name='TransactionEvent',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('recorded_at', models.DateTimeField(auto_now_add=True, help_text='When this log entry was recorded.')),
                ('action', models.CharField(choices=[('refund', 'Refund'), ('charge', 'Charge')], max_length=6)),
                ('amount_in_cents', models.IntegerField()),
                ('currency', looper.models.CurrencyField(choices=[('usd', 'USD'), ('eur', 'EUR')], default='eur', max_length=3)),
                ('status', models.CharField(choices=[('succeeded', 'Succeeded'), ('pending', 'Pending'), ('failed', 'Failed')], max_length=20)),
            ],
        ),
        migrations.DeleteModel(
            name='Event',
        ),
        migrations.RemoveField(
            model_name='invoice',
            name='order',
        ),
        migrations.RenameField(
            model_name='transaction',
            old_name='amunt_refunded',
            new_name='amount_refunded',
        ),
        migrations.RemoveField(
            model_name='order',
            name='alpha_id',
        ),
        migrations.RemoveField(
            model_name='subscription',
            name='activated_at',
        ),
        migrations.RemoveField(
            model_name='subscription',
            name='alpha_id',
        ),
        migrations.RemoveField(
            model_name='subscription',
            name='current_period_ends_at',
        ),
        migrations.RemoveField(
            model_name='subscription',
            name='current_period_started_at',
        ),
        migrations.RemoveField(
            model_name='subscription',
            name='expires_at',
        ),
        migrations.RemoveField(
            model_name='subscription',
            name='total_billing_cycles',
        ),
        migrations.RemoveField(
            model_name='transaction',
            name='captured',
        ),
        migrations.RemoveField(
            model_name='transaction',
            name='invoice',
        ),
        migrations.AddField(
            model_name='subscription',
            name='current_interval_ends_at',
            field=models.DateTimeField(help_text='When the next payment is due.', null=True),
        ),
        migrations.AddField(
            model_name='subscription',
            name='current_interval_started_at',
            field=models.DateTimeField(null=True),
        ),
        migrations.AddField(
            model_name='subscription',
            name='intervals_elapsed',
            field=models.IntegerField(blank=True, default=0, help_text='How many billing cycles have happened.'),
        ),
        migrations.AddField(
            model_name='subscription',
            name='started_at',
            field=models.DateTimeField(help_text='Date of first activation.', null=True),
        ),
        migrations.AddField(
            model_name='transaction',
            name='refunded_at',
            field=models.DateTimeField(null=True),
        ),
        migrations.AddField(
            model_name='transaction',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, verbose_name='date edited'),
        ),
        migrations.RenameField(
            model_name='address',
            old_name='country_code_alpha2',
            new_name='country',
        ),
        migrations.AlterField(
            model_name='address',
            name='country',
            field=django_countries.fields.CountryField(max_length=2),
        ),
        migrations.AlterField(
            model_name='order',
            name='collection_method',
            field=models.CharField(choices=[('automatic', 'Automatic'), ('manual', 'Manual (by subscriber)'), ('managed', 'Managed (manual by staff)')], default='automatic', max_length=20),
        ),
        migrations.AlterField(
            model_name='order',
            name='currency',
            field=looper.models.CurrencyField(choices=[('usd', 'USD'), ('eur', 'EUR')], default='eur', max_length=3),
        ),
        migrations.AlterField(
            model_name='order',
            name='price_in_cents',
            field=models.IntegerField(help_text='Including tax.'),
        ),
        migrations.AlterField(
            model_name='order',
            name='tax_in_cents',
            field=models.IntegerField(blank=True, default=0),
        ),
        migrations.AlterField(
            model_name='order',
            name='tax_region',
            field=django_countries.fields.CountryField(blank=True, default='', max_length=2),
        ),
        migrations.AlterField(
            model_name='order',
            name='tax_type',
            field=models.CharField(blank=True, default='', max_length=20),
        ),
        migrations.AlterField(
            model_name='planvariation',
            name='currency',
            field=looper.models.CurrencyField(choices=[('usd', 'USD'), ('eur', 'EUR')], default='eur', max_length=3),
        ),
        migrations.AlterField(
            model_name='planvariation',
            name='price_in_cents',
            field=models.IntegerField(help_text='Including tax.'),
        ),
        migrations.AlterField(
            model_name='subscription',
            name='canceled_at',
            field=models.DateTimeField(null=True),
        ),
        migrations.AlterField(
            model_name='subscription',
            name='collection_method',
            field=models.CharField(choices=[('automatic', 'Automatic'), ('manual', 'Manual (by subscriber)'), ('managed', 'Managed (manual by staff)')], default='automatic', max_length=20),
        ),
        migrations.AlterField(
            model_name='subscription',
            name='currency',
            field=looper.models.CurrencyField(choices=[('usd', 'USD'), ('eur', 'EUR')], default='eur', max_length=3),
        ),
        migrations.AlterField(
            model_name='subscription',
            name='interval_length',
            field=models.IntegerField(help_text='How many "interval units" is each billing cycle.'),
        ),
        migrations.AlterField(
            model_name='subscription',
            name='interval_unit',
            field=models.CharField(choices=[('day', 'Day'), ('week', 'Week'), ('month', 'Month'), ('year', 'Year')], default='day', help_text='The units in which "interval length" is expressed', max_length=50),
        ),
        migrations.AlterField(
            model_name='subscription',
            name='price_in_cents',
            field=models.IntegerField(help_text='Including tax.'),
        ),
        migrations.AlterField(
            model_name='subscription',
            name='status',
            field=models.CharField(choices=[('active', 'Active'), ('on_hold', 'On Hold'), ('canceled', 'Canceled')], default='on_hold', max_length=20),
        ),
        migrations.AlterField(
            model_name='subscription',
            name='tax_in_cents',
            field=models.IntegerField(blank=True, default=0),
        ),
        migrations.AlterField(
            model_name='subscription',
            name='tax_region',
            field=django_countries.fields.CountryField(blank=True, default='', max_length=2),
        ),
        migrations.AlterField(
            model_name='subscription',
            name='tax_type',
            field=models.CharField(blank=True, default='', max_length=20),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='currency',
            field=looper.models.CurrencyField(choices=[('usd', 'USD'), ('eur', 'EUR')], default='eur', max_length=3),
        ),
        migrations.DeleteModel(
            name='Invoice',
        ),
        migrations.AddField(
            model_name='transactionevent',
            name='transaction',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='events', to='looper.Transaction'),
        ),
        migrations.RemoveField(
            model_name='order',
            name='ip_address',
        ),
        migrations.AddField(
            model_name='transaction',
            name='ip_address',
            field=models.GenericIPAddressField(help_text='IP address of the user at the moment of paying', null=True),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, verbose_name='date created'),
        ),
    ]
